package com.aht.agilspace.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model){
        return "login";
    }
    
    @RequestMapping(value = {"/","/index"}, method = RequestMethod.GET)
    public String home(Model model){
        return "index";
    }
    
    @RequestMapping(value = "/admin/profile", method = RequestMethod.GET)
    public String profile(Model model){
    	return "profile";
    }
    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public String error(Model model){
    	return "error";
    }
}
