package com.aht.agilspace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgilspaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgilspaceApplication.class, args);
	}

}

