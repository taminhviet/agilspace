package com.aht.agilspace.util;

public class Constants {

	public static final int ROLE_ADMIN = 1;
	public static final String ADMIN = "ADMIN";
	public static final String DEFAULT_AUTHEN = "admin";
	public static final String DEFAULT_EMAIL = "sale@agilspace.com";
	public static final String DEFAULT_PHONE = "(65) 6521 7888";
	public static final String DEFAULT_ADDRESS = "1 Ang Mo Kio Electronics Park Road #06-02 Singapore 567710";
}
