package com.aht.agilspace.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.aht.agilspace.model.User;
import com.aht.agilspace.service.UserService;
import com.aht.agilspace.util.Constants;

@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent>{
	
	@Autowired
	UserService userService;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		
		//initial only 1 user admin
		List<User> lsUser = userService.getAllUser();
		if(lsUser == null || lsUser.isEmpty()){
			User admin = new User();
			admin.setId(1);
			admin.setUsername(Constants.DEFAULT_AUTHEN);
			admin.setPassword(Constants.DEFAULT_AUTHEN);
			admin.setIsAdmin(Constants.ROLE_ADMIN);
			admin.setEmail(Constants.DEFAULT_EMAIL);
			admin.setPhone(Constants.DEFAULT_PHONE);
			admin.setAddress(Constants.DEFAULT_ADDRESS);
			userService.saveUser(admin);
		}
	}

}
